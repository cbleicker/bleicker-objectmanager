<?php

namespace Bleicker\ObjectManager\Exception;

use Bleicker\Exception\ThrowableException as Exception;

/**
 * Class ExistingClassOrInterfaceNameExpectedException
 *
 * @package Bleicker\ObjectManager\Exception
 */
class ExistingClassOrInterfaceNameExpectedException extends Exception {

}
