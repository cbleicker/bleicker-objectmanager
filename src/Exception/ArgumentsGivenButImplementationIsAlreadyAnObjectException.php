<?php

namespace Bleicker\ObjectManager\Exception;

use Bleicker\Exception\ThrowableException as Exception;

/**
 * Class ArgumentsGivenButImplementationIsAlreadyAnObjectException
 *
 * @package Bleicker\ObjectManager\Exception
 */
class ArgumentsGivenButImplementationIsAlreadyAnObjectException extends Exception {

}
